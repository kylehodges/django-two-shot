from django.urls import path
from receipts.views import home, create_receipt, category_list, account_list, create_category
from accounts.views import create_account


urlpatterns = [
    path('', home, name='home'),
    path('create/', create_receipt, name='create_receipt'),
    path('categories/', category_list, name='categories_list'),
    path('accounts/', account_list, name='account_list'),
    path('categories/create/', create_category, name='create_category'),
    path('accounts/create/', create_account, name='create_account'),
]
