from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory


class CreateReceiptFrom(ModelForm):
    
    class Meta:
        model = Receipt
        fields = ("vendor", "total", "tax", "date", "category", "account")

class CreateExpenseCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ('name',)
