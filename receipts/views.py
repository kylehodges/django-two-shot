from django.shortcuts import render, redirect
from receipts.models import ExpenseCategory, Receipt, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import CreateReceiptFrom, CreateExpenseCategoryForm

# Create your views here.

@login_required
def home(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    for record in receipts:
        record.date = record.date.strftime('%m/%d/%y')

    context = {
        'reciepts': receipts
    }
    return render(request, 'receipts/home.html', context)

@login_required
def create_receipt(request):
    if request.method == 'GET':
        form = CreateReceiptFrom()

    else:
        form = CreateReceiptFrom(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')

    context = {
        'form': form
    }
    return render(request, 'receipts/create.html', context)

@login_required
def category_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    categories = ExpenseCategory.objects.filter(owner=request.user)

    context = {
        'receipts': receipts,
        'categories': categories

    }
    return render(request, 'receipts/categories.html', context)

@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)

    context = {
        'accounts': accounts,
    }

    return render(request, 'receipts/accounts.html', context)

@login_required
def create_category(request):
    if request.method == 'GET':
        form = CreateExpenseCategoryForm()

    else:
        form = CreateExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect('categories_list')

    context = {
        'form': form
    }

    return render(request, 'receipts/create_category.html', context)
