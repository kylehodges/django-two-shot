from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from accounts.forms import LoginForm, SignUpForm, CreateAccoutForm
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from receipts.models import Account
# Create your views here.


def user_login(request):
    if request.method == 'GET':
        form = LoginForm()

    else:
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(
                request,
                username=username,
                password=password
            )

            if user is not None:
                login(request, user)
                return redirect('home')

    context = {
        'form': form
    }
    return render(request, 'accounts/login.html', context)

def user_logout(request):
    logout(request)
    return redirect('login')

def sign_up_user(request):
    if request.method == 'GET':
        form = SignUpForm()
    else:
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            password_confirmation = form.cleaned_data['password_confirmation']

            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password=password
                )
                login(request, user)
                return redirect('home')
            else:
                form.add_error('password', "the passwords do not match")

    context = {
        'form': form
    }
    return render(request, 'accounts/signup.html', context)

@login_required
def create_account(request):
    if request.method == 'GET':
        form = CreateAccoutForm()

    else:
        form = CreateAccoutForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect('account_list')

    context = {
        'form': form
    }

    return render(request, 'receipts/create_account.html', context)
